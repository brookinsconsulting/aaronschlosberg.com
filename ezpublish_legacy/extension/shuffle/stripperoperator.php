<?php

/*! \file stripperoperator.php
*/

/*!
  \class stripperOperator stripperoperator.php
  \brief The class stripperoperator gets a string and deletes the html-tags.
  \author Sören Meyer@xrow.de
*/
class stripperOperator
{
    var $Operators;

    function stripperOperator( $name = "stripper" )
    {
	$this->Operators = array( $name );
    }

    /*! Returns the template operators.
    */
    function &operatorList()
    {
	return $this->Operators;
    }

    function modify( &$tpl, &$operatorName, &$operatorParameters, &$rootNamespace, &$currentNamespace, &$operatorValue, &$namedParameters )
    {
	$operatorValue=strip_tags($operatorValue);
    }
}

?>
