{* Image - Gallery line view *}
<div class="content-view-galleryline">
    <div class="class-image">

    <div class="attribute-image">

    <a href={$node.object.data_map.image.object.data_map.image.content[imagelarge].url|ezroot()} rel="lightbox-gallery" title="{$node.object.data_map.name.content|wash()} {$node.object.data_map.caption.content.output.output_text|wash()}"><img src={$node.object.data_map.image.object.data_map.image.content[gallerythumbnail].url|ezroot()}  alt="{$node.object.data_map.name.content|wash()}" title="{$node.object.data_map.name.content|wash()}" /></a>    

    </div>
    <div class="attribute-caption">
        <p>{attribute_view_gui attribute=$node.data_map.name}</p>
        {if $node.object.data_map.caption.has_content}
        <p>{attribute_view_gui attribute=$node.object.data_map.caption}</p>
        {/if}
    </div>
    </div>
</div>
