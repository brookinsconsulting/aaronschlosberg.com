<?php /* #?ini charset="utf-8"?

[ExtensionSettings]
ActiveAccessExtensions[]
ActiveAccessExtensions[]=ezlightbox
ActiveAccessExtensions[]=illightbox2
ActiveAccessExtensions[]=aaronschlosberg
ActiveAccessExtensions[]=bcwebsitestatistics
ActiveAccessExtensions[]=bccontentdiffnotifications
ActiveAccessExtensions[]=ezwebin
ActiveAccessExtensions[]=shuffle
# ActiveAccessExtensions[]=eznewsletter
# ActiveAccessExtensions[]=eznewsletter_builder
# ActiveAccessExtensions[]=ezapprove2

[DatabaseSettings]
DatabaseImplementation=ezmysql
Server=localhost
User=asdev
Password=publish
Database=asdev
Charset=
Socket=disabled

[InformationCollectionSettings]
EmailReceiver=info@brookinsconsutling.com

[Session]
SessionNamePerSiteAccess=disabled

[SiteSettings]
SiteName=Aaron Schlosberg
SiteURL=dev.aaronschlosberg.com
LoginPage=embedded
AdditionalLoginFormActionURL=http://edit.dev.aaronschlosberg.com/user/login

[UserSettings]
RegistrationEmail=info@brookinsconsutling.com

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]
RelatedSiteAccessList[]=aaronschlosberg_site_user
RelatedSiteAccessList[]=aaronschlosberg_site_admin
# RelatedSiteAccessList[]=eng
# RelatedSiteAccessList[]=ezwebin_site_user
# RelatedSiteAccessList[]=ezwebin_site_admin
ShowHiddenNodes=false

[DesignSettings]
SiteDesign=ezwebin
# SiteDesign=ezwebin|||aaronschlosberg
AdditionalSiteDesignList[]
AdditionalSiteDesignList[]=base
AdditionalSiteDesignList[]=aaronschlosberg

[RegionalSettings]
Locale=eng-US
ContentObjectLocale=eng-US
ShowUntranslatedObjects=disabled
SiteLanguageList[]
SiteLanguageList[]=eng-US

# TextTranslation=enabled
TextTranslation=disabled

[FileSettings]
# VarDir=var/ezwebin_site
VarDir=var/aaronschlosberg_site

[ContentSettings]
TranslationList=

[MailSettings]
ContentType=text/html
AdminEmail=info@brookinsconsulting.com
EmailSender=info@brookinsconsutling.com

[DebugSettings]
DebugByIP=disabled
# DebugByIP=enabled
# DebugIPList[]=78.105.99.97
# DebugOutput=enabled
DebugOutput=disabled
# DebugRedirection=disabled
# Developer toolbar with clear cache and quick settings features
DebugToolbar=disabled
QuickSettingsList[]


[ContentSettings]
ViewCaching=disabled

[TemplateSettings]
TemplateCache=disabled
TemplateCompile=disabled
# DevelopmentMode=disabled
DevelopmentMode=enabled
CacheThreshold=0
# ShowUsedTemplates=enabled
# TemplateOptimization=enabled
# Debug=enabled
# DebugOutput=enabled
ShowUsedTemplates=enabled


*/ ?>