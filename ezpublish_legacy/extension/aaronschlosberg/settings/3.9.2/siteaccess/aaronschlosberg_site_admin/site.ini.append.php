<?php /* #?ini charset="utf-8"?

[ExtensionSettings]
ActiveAccessExtensions[]
ActiveAccessExtensions[]=ezlightbox


[DatabaseSettings]
DatabaseImplementation=ezmysql
Server=localhost
User=asdev
Password=publish
Database=asdev
Charset=
Socket=disabled

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteName=Aaron Schlosberg
SiteURL=edit.dev.aaronschlosberg.com
LoginPage=custom

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=true
RelatedSiteAccessList[]=aaronschlosberg_site_user
RelatedSiteAccessList[]=aaronschlosberg_site_admin
RelatedSiteAccessList[]=eng
RelatedSiteAccessList[]=ezwebin_site_user
RelatedSiteAccessList[]=ezwebin_site_admin
ShowHiddenNodes=true

[DesignSettings]
SiteDesign=aaronschlosberg_site_admin
# SiteDesign=ezwebin_site_admin
AdditionalSiteDesignList[]=admin
# AdditionalSiteDesignList[]=aaronschlosberg_admin

[RegionalSettings]
Locale=eng-US
ContentObjectLocale=eng-US
ShowUntranslatedObjects=enabled
SiteLanguageList[]=eng-US

# TextTranslation=enabled
TextTranslation=disabled

[FileSettings]
# VarDir=var/ezwebin_site
VarDir=var/aaronschlosberg_site

[ContentSettings]
CachedViewPreferences[full]=admin_navigation_content=0;admin_navigation_details=0;admin_navigation_languages=0;admin_navigation_locations=0;admin_navigation_relations=0;admin_navigation_roles=0;admin_navigation_policies=0;admin_navigation_content=0;admin_navigation_translations=0;admin_children_viewmode=list;admin_list_limit=1;admin_edit_show_locations=0;admin_leftmenu_width=10;admin_url_list_limit=10;admin_url_view_limit=10;admin_section_list_limit=1;admin_orderlist_sortfield=user_name;admin_orderlist_sortorder=desc;admin_search_stats_limit=1;admin_treemenu=1;admin_bookmarkmenu=1;admin_left_menu_width=13
TranslationList=

[MailSettings]
AdminEmail=info@brookinsconsulting.com
EmailSender=
*/ ?>