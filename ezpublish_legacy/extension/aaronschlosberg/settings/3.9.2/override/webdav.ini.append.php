<?php /* #?ini charset="utf-8"?

[GeneralSettings]
EnableWebDAV=true
Logging=enabled

# List of classes that should always be seen as a folder.
# By default, if a class contains an image attribute it is seen as an image.
FolderClasses[]
FolderClasses[]=folder
FolderClasses[]=article
FolderClasses[]=gallery
FolderClasses[]=frontpage
# FolderClasses[]=
# FolderClasses[]=your_custom_class

*/ ?>