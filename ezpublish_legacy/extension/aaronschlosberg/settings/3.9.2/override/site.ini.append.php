<?php /* #?ini charset="utf-8"?

[ExtensionSettings]
ActiveExtensions[]
ActiveExtensions[]=ezdhtml
ActiveExtensions[]=ezodf

[Session]
SessionNameHandler=custom

[SiteSettings]
DefaultAccess=aaronschlosberg_site_user
SiteList[]
SiteList[]=aaronschlosberg_site_user
SiteList[]=aaronschlosberg_site_admin
RootNodeDepth=1
SiteName=Aaron Schlosberg
MetaDataArray[author]=Aaron Schlosberg
MetaDataArray[copyright]=Aaron Schlosberg
MetaDataArray[description]=Artist Aaron Schlosberg
MetaDataArray[keywords]=Artist,Painter,Musician,Presenter
SiteURL=dev.aaronschlosberg.com

[UserSettings]
LogoutRedirect=/

[SiteAccessSettings]
CheckValidity=false
AvailableSiteAccessList[]
AvailableSiteAccessList[]=aaronschlosberg_site_user
AvailableSiteAccessList[]=aaronschlosberg_site_admin
# AvailableSiteAccessList[]=aaronschlosberg_site_blog
MatchOrder=host
HostMatchMapItems[]
HostMatchMapItems[]=dev.aaronschlosberg.com;aaronschlosberg_site_user
HostMatchMapItems[]=admin.dev.aaronschlosberg.com;aaronschlosberg_site_admin
HostMatchMapItems[]=edit.dev.aaronschlosberg.com;aaronschlosberg_site_admin
HostMatchMapItems[]=webdav.dev.aaronschlosberg.com;aaronschlosberg_site_admin
HostMatchMapItems[]=blog.aaronschlosberg.com;aaronschlosberg_site_blog

[FileSettings]
VarDir=var/aaronschlosberg_site

[MailSettings]
Transport=sendmail
AdminEmail=info@brookinsconsulting.com
EmailSender=

[EmbedViewModeSettings]
# AvailableViewModes[]
AvailableViewModes[]=embed
AvailableViewModes[]=embed-inline
InlineViewModes[]
InlineViewModes[]=embed-inline
*/ ?>