<?php /* #?ini charset="utf-8"?

[ExtensionSettings]
ActiveAccessExtensions[]
ActiveAccessExtensions[]=aaronschlosberg
ActiveAccessExtensions[]=ezwebin
ActiveAccessExtensions[]=shuffle
ActiveAccessExtensions[]=ezsmoothgallery
ActiveAccessExtensions[]=ezlightbox
ActiveAccessExtensions[]=ezmultiupload

##
# ActiveAccessExtensions[]=bcwebsitestatistics
# ActiveAccessExtensions[]=bccontentdiffnotifications
# ActiveAccessExtensions[]=illightbox2
# ActiveAccessExtensions[]=eznewsletter
# ActiveAccessExtensions[]=eznewsletter_builder
# ActiveAccessExtensions[]=ezapprove2

[DatabaseSettings]
DatabaseImplementation=ezmysql
Server=localhost
## Server=127.0.0.1
Port=
User=db
Password=db
Database=aaronschlosberg
Charset=
Socket=disabled

[InformationCollectionSettings]
EmailReceiver=aaronschlosberg@gmail.com

[Session]
SessionNamePerSiteAccess=disabled

[SiteSettings]
SiteName=Aaron Schlosberg
SiteURL=aaronschlosberg.com
LoginPage=embedded
# AdditionalLoginFormActionURL=http://ezwebin_site_admin/ez/index.php/ezwebin_site_admin/user/login
AdditionalLoginFormActionURL=http://edit.dev.aaronschlosberg.com/user/login

[UserSettings]
RegistrationEmail=aaronschlosberg@gmail.com

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]
RelatedSiteAccessList[]=ezwebin_site
RelatedSiteAccessList[]=eng
RelatedSiteAccessList[]=ezwebin_site_admin
RelatedSiteAccessList[]=aaronschlosberg
RelatedSiteAccessList[]=aaronschlosbergadmin
ShowHiddenNodes=false

[DesignSettings]
# SiteDesign=ezwebin
SiteDesign=aaronschlosberg
AdditionalSiteDesignList[]
AdditionalSiteDesignList[]=aaronschlosberg
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=standard
AdditionalSiteDesignList[]=base

[RegionalSettings]
Locale=eng-US
ContentObjectLocale=eng-US
ShowUntranslatedObjects=disabled
SiteLanguageList[]
SiteLanguageList[]=eng-US
TextTranslation=disabled

[FileSettings]
# VarDir=var/ezwebin_site
# VarDir=var/aaronschloshberg_site
VarDir=var/aaronschloshberg

[ContentSettings]
TranslationList=

[MailSettings]
AdminEmail=aaronschlosberg@gmail.com
EmailSender=aaron@aaronschlosberg.com

[DebugSettings]
# DebugByIP=disabled
# DebugByIP=enabled
# DebugIPList[]=78.105.99.97
DebugOutput=enabled
# DebugOutput=disabled
# DebugRedirection=disabled
# Developer toolbar with clear cache and quick settings features
# DebugToolbar=disabled
# QuickSettingsList[]

[ContentSettings]
##ViewCaching=disabled

[TemplateSettings]
##TemplateCache=disabled
##TemplateCompile=disabled
# DevelopmentMode=disabled
##DevelopmentMode=enabled
##CacheThreshold=0
# TemplateOptimization=enabled
# Debug=enabled
# Debug=disabled
# DebugOutput=enabled
ShowUsedTemplates=enabled
##ShowXHTMLCode=enabled

*/ ?>
