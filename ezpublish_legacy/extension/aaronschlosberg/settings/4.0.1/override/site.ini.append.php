<?php /* #?ini charset="utf-8"?

[ExtensionSettings]
ActiveExtensions[]=ezoe
ActiveExtensions[]=ezodf
ActiveExtensions[]=ezwt

[Session]
SessionNameHandler=custom

[SiteSettings]
DefaultAccess=aaronschlosberg
SiteList[]=aaronschlosberg
SiteList[]=aaronschlosbergadmin
RootNodeDepth=1
SiteURL=aaronschlosberg.com
SiteName=Aaron Schlosberg
MetaDataArray[author]=Aaron Schlosberg
MetaDataArray[copyright]=Aaron Schlosberg
MetaDataArray[description]=Artist Aaron Schlosberg
MetaDataArray[keywords]=Artist,Painter,Musician,Presenter,Student

[UserSettings]
LogoutRedirect=/

[SiteAccessSettings]
CheckValidity=true
AvailableSiteAccessList[]=aaronschlosberg
AvailableSiteAccessList[]=aaronschlosbergadmin
ForceVirtualHost=true
MatchOrder=host;uri
HostMatchMapItems[]=aaronschlosberg.com;aaronschlosberg
HostMatchMapItems[]=www.aaronschlosberg.com;aaronschlosberg
HostMatchMapItems[]=admin.aaronschlosberg.com;aaronschlosbergadmin
HostMatchMapItems[]=edit.aaronschlosberg.com;aaronschlosbergadmin
HostMatchMapItems[]=dev.aaronschlosberg.com;aaronschlosberg
HostMatchMapItems[]=admin.dev.aaronschlosberg.com;aaronschlosbergadmin

[FileSettings]
VarDir=var/aaronschlosberg

[MailSettings]
ContentType=text/html
Transport=sendmail
AdminEmail=aaronschlosberg@gmail.com
EmailSender=aaron@aaronschlosberg.com

[EmbedViewModeSettings]
AvailableViewModes[]=embed
AvailableViewModes[]=embed-inline
InlineViewModes[]=embed-inline

[TemplateSettings]
DevelopmentMode=disabled

[DebugSettings]
DebugOutput=disabled

[RegionalSettings]
Locale=eng-US
ContentObjectLocale=eng-US
ShowUntranslatedObjects=disabled
SiteLanguageList[]=eng-US
TextTranslation=enabled
*/ ?>