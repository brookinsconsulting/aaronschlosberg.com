<?php /* #?ini charset="utf-8"?

[ExtensionSettings]
ActiveAccessExtensions[]
# ActiveAccessExtensions[]=aaronschlosberg
ActiveAccessExtensions[]=ezwebin
# ActiveAccessExtensions[]=ezsmoothgallery
ActiveAccessExtensions[]=ezmultiupload

[DatabaseSettings]
DatabaseImplementation=ezmysqli
# Server=localhost
Server=localhost
Port=
User=db
Password=db
Database=aaronschlosberg
Charset=
Socket=disabled

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteName=Aaron Schlosberg Admin
SiteURL=admin.aaronschlosberg.com
LoginPage=custom
DefaultPage=content/dashboard

[UserSettings]
RegistrationEmail=aaron@aaronschlosberg.com

[SiteAccessSettings]
RequireUserLogin=true
RelatedSiteAccessList[]=aaronschlosberg
RelatedSiteAccessList[]=aaronschlosbergadmin
ShowHiddenNodes=true

[DesignSettings]
SiteDesign=ezwebin_site_admin
AdditionalSiteDesignList[]=admin2
AdditionalSiteDesignList[]=admin

[RegionalSettings]
Locale=eng-US
ContentObjectLocale=eng-US
ShowUntranslatedObjects=enabled
SiteLanguageList[]=eng-US
TextTranslation=enabled
# TextTranslation=disabled

[FileSettings]
# VarDir=var/ezwebin_site
VarDir=var/aaronschlosberg

[ContentSettings]
CachedViewPreferences[full]=admin_navigation_content=1;admin_children_viewmode=list;admin_list_limit=1
TranslationList=

[MailSettings]
AdminEmail=info@brookinsconsulting.com
EmailSender=info@brookinsconsulting.com

*/ ?>