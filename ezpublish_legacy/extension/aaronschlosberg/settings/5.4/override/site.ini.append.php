<?php /* #?ini charset="utf-8"?

[ExtensionSettings]
ActiveExtensions[]
ActiveExtensions[]=ezjscore
ActiveExtensions[]=ezoe
# ActiveExtensions[]=ezformtoken
ActiveExtensions[]=ezwt
# ActiveExtensions[]=ezstarrating
# ActiveExtensions[]=ezgmaplocation
ActiveExtensions[]=ezwebin
ActiveExtensions[]=ezie
ActiveExtensions[]=ezodf
# ActiveExtensions[]=ezprestapiprovider
ActiveExtensions[]=ezmultiupload
ActiveExtensions[]=eztags
ActiveExtensions[]=ezautosave
ActiveExtensions[]=bcgooglesitemaps

[Session]
SessionNameHandler=custom

[SiteSettings]
DefaultAccess=aaronschlosberg
SiteList[]
SiteList[]=aaronschlosberg
SiteList[]=aaronschlosbergadmin
SiteList[]=ezwebin_site_admin
RootNodeDepth=1
SiteURL=aaronschlosberg.com
SiteName=Aaron Schlosberg
MetaDataArray[author]=Aaron Schlosberg
MetaDataArray[copyright]=Aaron Schlosberg
MetaDataArray[description]=Artist Aaron Schlosberg
MetaDataArray[keywords]=Artist,Painter,Musician,Presenter,Student

[UserSettings]
LogoutRedirect=/

[SiteAccessSettings]
CheckValidity=false
AvailableSiteAccessList[]
AvailableSiteAccessList[]=aaronschlosberg
AvailableSiteAccessList[]=aaronschlosbergadmin
AvailableSiteAccessList[]=ezwebin_site_admin
ForceVirtualHost=true
MatchOrder=host;uri
HostMatchMapItems[]=aaronschlosberg.com;aaronschlosberg
HostMatchMapItems[]=www.aaronschlosberg.com;aaronschlosberg
HostMatchMapItems[]=admin.aaronschlosberg.com;aaronschlosbergadmin
HostMatchMapItems[]=edit.aaronschlosberg.com;aaronschlosbergadmin
HostMatchMapItems[]=dev.aaronschlosberg.com;aaronschlosberg
HostMatchMapItems[]=admin.dev.aaronschlosberg.com;aaronschlosbergadmin

[DesignSettings]
DesignLocationCache=enabled

[RegionalSettings]
TranslationSA[]
TranslationSA[eng]=Eng

[FileSettings]
VarDir=var/aaronschlosberg

[MailSettings]
Transport=sendmail
AdminEmail=info@brookinsconsulting.com
EmailSender=

[EmbedViewModeSettings]
AvailableViewModes[]
AvailableViewModes[]=embed
AvailableViewModes[]=embed-inline
InlineViewModes[]
InlineViewModes[]=embed-inline

*/ ?>