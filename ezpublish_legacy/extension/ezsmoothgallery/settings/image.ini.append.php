<?php /* #?ini charset="utf-8"?

[AliasSettings]
AliasList[]=image_galerie
AliasList[]=vignette_galerie
# Vignette = Thumb

[image_galerie]
Reference=original
Filters[]=geometry/scaledownonly=480;400

[vignette_galerie]
Reference=original
Filters[]=geometry/scaledownonly=150;150
*/

?>
