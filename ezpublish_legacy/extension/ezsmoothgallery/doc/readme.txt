============
WHAT IS IT ?
============

eZ Smoothgallery provides a quick and ready-to-run image gallery model. 
It has several advantages over flash galleries. 
- Images remains viewable if javascript is disabled. 
- Images can be indexed by search engines.

It requires regular "Image" content class. With some customization, it can used with any content class that uses a image datatype.

Developped on Ezpublish 3.9.
Tested with Internet Explorer 7 and Firefox 2.0. 
Any feedback is welcome : nal@ingenieursetconsultants.com

===========
CHANGELOG
===========
09/12/07
- Initial release

===========
WHAT'S DONE
===========
- Standard behaviour with images browsing.

===========
TODO
===========

- Add parameter to transform the gallery into automatic slideshow.


Copyright 2007 Alimi Nabil <starnab@hotmail.com> http://www.starnab.com