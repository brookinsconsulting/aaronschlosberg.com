<?php
class ezdebugInfo
{
    static function info()
    {
        return array(
            'Name' => "Batchtool",
            'Version' => "1.01",
            'Copyright' => "Copyright (C) 2008 A.Bakkeboe",
            'License' => "GNU General Public License v2.0"
        );
    }
}
?>
