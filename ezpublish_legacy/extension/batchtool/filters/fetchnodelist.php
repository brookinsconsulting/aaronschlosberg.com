<?php

class fetchnodelistFilter
{
    // Return help text for this filter
    function getHelpText()
    {
        return '
--filter="fetchnodelist;parent=<parent>[;classname=<class id list>][;limit=<limit>][;offset=<offset>]"

parent - The parent node id of the nodes you want to fetch
classname - A list of class ids separated by a colon
limit - Limit the total number of nodes to fetch
offset - The offset value for the node in the list of nodes to fetch
';
    }

    // Sets required and optional command line parameter fields for this class
    // If any values are wrong or missing, return an error message as string
    // Otherwise return true
    function setParameters( $parm_array )
    {
        $this->parent_node_id = intval( $parm_array['parent'] );
        if ( $this->parent_node_id == 0 )
            return 'Missing or illegal parent node id';
        $this->limit = intval( $parm_array[ 'limit' ] );
        $this->offset = intval( $parm_array[ 'offset' ] );
        $this->class_filter = isset( $parm_array['classname'] ) ? explode( ':', $parm_array['classname'] ) : array();
        return true;
    }
    
    // Returns name of the ID attribute in the returned objects
    // This function is not required, will return 'node_id' if not available
    function getIDField()
    {
        return 'node_id';
    }

    // Returns an array of objects to do operations on
    // All filters in a job must return the same type of objects,
    // which must correspond to the type of objects operations in a job is made for
    function getObjectList()
    {
        return eZFunctionHandler::execute( 'content', 'list', 
                array( 'parent_node_id' => $this->parent_node_id, 
                        'class_filter_type' => 'include', 
                        'class_filter_array' => $this->class_filter,
                        'limit' => $this->limit,
                        'offset' => $this->offset
                        ) );
    }

// Command line input parameters
var $parent_node_id;
var $class_filter;
var $limit;
var $offset;
}

?>
