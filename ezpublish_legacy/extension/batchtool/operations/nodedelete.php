<?php

class nodedeleteOperation
{
    // Return help text for this filter
    function getHelpText()
    {
        return '
--operation="nodedelete"

Will delete the selected nodes.
The trash can will not be used, object is irrevocably removed from the database.
';
    }

    function setParameters( $parm_array )
    {
        return true;
    }

    // Delete the given node and it's object
    // Object is completely removed, not put in trash!
    function runOperation( &$object )
    {
        return eZContentObjectOperations::remove( $object->attribute( 'contentobject_id' ) );
    }
}

?>
