<?php /* #?ini charset="utf-8"?

[DatabaseSettings]
DatabaseImplementation=ezmysqli
Server=localhost
Port=
User=db
Password=db
Database=aaronschlosberg_54
Charset=
Socket=disabled

[InformationCollectionSettings]
EmailReceiver=

[Session]
SessionNamePerSiteAccess=disabled

[SiteSettings]
SiteName=Aaron Schlosberg
SiteURL=admin.aaronschlosberg.com
LoginPage=embedded
AdditionalLoginFormActionURL=http://admin.aaronschlosberg.com/index.php/user/login

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]
RelatedSiteAccessList[]=ezdemo_site_user
RelatedSiteAccessList[]=eng
RelatedSiteAccessList[]=ezdemo_site_admin
ShowHiddenNodes=false

[DesignSettings]
SiteDesign=ezdemo
AdditionalSiteDesignList[]
AdditionalSiteDesignList[]=ezflow
AdditionalSiteDesignList[]=base
AdditionalSiteDesignList[]=standard

[RegionalSettings]
Locale=eng-US
ContentObjectLocale=eng-US
ShowUntranslatedObjects=disabled
SiteLanguageList[]
SiteLanguageList[]=eng-US
TextTranslation=enabled

[FileSettings]
VarDir=var/ezdemo_site

[ContentSettings]
TranslationList=

[MailSettings]
AdminEmail=info@brookinsconsulting.com
EmailSender=
*/ ?>