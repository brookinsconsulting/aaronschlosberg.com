<?php /* #?ini charset="utf-8"?

[DatabaseSettings]
DatabaseImplementation=ezmysqli
Server=localhost
Port=
User=db
Password=db
Database=aaronschlosberg_54
Charset=
Socket=disabled

[InformationCollectionSettings]
EmailReceiver=

[Session]
SessionNamePerSiteAccess=disabled

[SiteSettings]
SiteName=Aaron Schlosberg
SiteURL=eng.1/index.php
LoginPage=embedded
AdditionalLoginFormActionURL=http://admin.aaronschlosberg.com/index.php/user/login

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]
RelatedSiteAccessList[]=ezwebin_site_user
RelatedSiteAccessList[]=eng
RelatedSiteAccessList[]=ezwebin_site_admin
ShowHiddenNodes=false

[DesignSettings]
SiteDesign=ezwebin
AdditionalSiteDesignList[]
AdditionalSiteDesignList[]=base

[RegionalSettings]
Locale=eng-US
ContentObjectLocale=eng-US
ShowUntranslatedObjects=disabled
SiteLanguageList[]
SiteLanguageList[]=eng-US
TextTranslation=enabled

[FileSettings]
VarDir=var/ezwebin_site

[ContentSettings]
TranslationList=

[MailSettings]
AdminEmail=info@brookinsconsulting.com
EmailSender=
*/ ?>