<?php /* #?ini charset="utf-8"?

[DatabaseSettings]
DatabaseImplementation=ezmysqli
Server=localhost
Port=
User=db
Password=db
Database=aaronschlosberg_54
Charset=
Socket=disabled

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteName=Aaron Schlosberg
SiteURL=admin.aaronschlosberg.com
DefaultPage=content/dashboard
LoginPage=custom

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=true
RelatedSiteAccessList[]=ezwebin_site_user
RelatedSiteAccessList[]=eng
RelatedSiteAccessList[]=ezwebin_site_admin
ShowHiddenNodes=true

[DesignSettings]
SiteDesign=ezwebin_site_admin
AdditionalSiteDesignList[]=admin2
AdditionalSiteDesignList[]=admin

[RegionalSettings]
Locale=eng-US
ContentObjectLocale=eng-US
ShowUntranslatedObjects=enabled
SiteLanguageList[]=eng-US
TextTranslation=enabled

[FileSettings]
VarDir=var/ezwebin_site

[ContentSettings]
CachedViewPreferences[full]=admin_navigation_content=1;admin_children_viewmode=list;admin_list_limit=1
TranslationList=

[MailSettings]
AdminEmail=info@brookinsconsulting.com
EmailSender=
*/ ?>