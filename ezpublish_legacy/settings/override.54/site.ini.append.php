<?php /* #?ini charset="utf-8"?

[ExtensionSettings]
ActiveExtensions[]
ActiveExtensions[]=ezjscore
ActiveExtensions[]=ezoe
ActiveExtensions[]=ezformtoken
ActiveExtensions[]=ezwt
ActiveExtensions[]=ezstarrating
ActiveExtensions[]=ezgmaplocation
ActiveExtensions[]=ezwebin
ActiveExtensions[]=ezie
ActiveExtensions[]=ezodf
ActiveExtensions[]=ezprestapiprovider
ActiveExtensions[]=ezmultiupload
ActiveExtensions[]=eztags
ActiveExtensions[]=ezautosave

[Session]
SessionNameHandler=custom

[SiteSettings]
DefaultAccess=eng
SiteList[]
SiteList[]=ezwebin_site_user
SiteList[]=eng
SiteList[]=ezwebin_site_admin
RootNodeDepth=1

[UserSettings]
LogoutRedirect=/

[SiteAccessSettings]
CheckValidity=false
AvailableSiteAccessList[]
AvailableSiteAccessList[]=ezwebin_site_user
AvailableSiteAccessList[]=eng
AvailableSiteAccessList[]=ezwebin_site_admin
MatchOrder=host
HostMatchMapItems[]
HostMatchMapItems[]=admin.aaronschlosberg.com;ezwebin_site_admin
HostMatchMapItems[]=aaronschlosberg.com;ezwebin_site_user
HostMatchMapItems[]=eng.1;eng

[DesignSettings]
DesignLocationCache=enabled

[RegionalSettings]
TranslationSA[]
TranslationSA[eng]=Eng

[FileSettings]
VarDir=var/ezwebin_site

[MailSettings]
Transport=sendmail
AdminEmail=info@brookinsconsulting.com
EmailSender=

[EmbedViewModeSettings]
AvailableViewModes[]
AvailableViewModes[]=embed
AvailableViewModes[]=embed-inline
InlineViewModes[]
InlineViewModes[]=embed-inline
*/ ?>